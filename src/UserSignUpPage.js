import React from 'react'
import axios from 'axios'
class UserSignUpPage extends React.Component{

state={
    username:null,
    displayname:null,
    password:null,
    passwordrepeat:null
}

onChange = event => {
    const{name,value}=event.target;
    this.setState({
      [name]:value      
    });
};

onClickSignUp = event => {
    event.preventDefault();
    console.log("Clicked")
    const {username,displayname,password}=this.state;
    const body = {
        username,
        displayname,
        password
    };
    axios.post('/api/1.0/users',body)
}
    render(){
        return (
            <div className="container">
           <form>     
            <h1 className="text-center">Sign Up</h1>
            <div className="form-group">
            <label>Username</label>
            <input className="form-control" name="username" onChange={this.onChange}></input>
            </div>
            <div className="form-group">
            <label>Displayname</label>
            <input className="form-control" name="displayname" onChange={this.onChange}></input>
            </div>
            <div className="form-group">
            <label>Password</label>
            <input className="form-control" name="password" type="Password" onChange={this.onChange}></input>
            </div>
            <div className="form-group">
            <label>Password Repeat</label>
            <input className="form-control" name="passwordrepeat" type="Password" onChange={this.onChange}></input>
            </div>
            <div className="text-center">
            <button className="btn btn-primary" onClick={this.onClickSignUp}>Submit</button>
            </div>
            </form>
            </div>    
        );
    }
}


export default UserSignUpPage